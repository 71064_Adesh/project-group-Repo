package com.project.springboothotelproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootHotelProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootHotelProjectApplication.class, args);
	}

}
